/*
 * conffiles.h
 *
 * module for registering, deregistering, and otherwise handling conffiles
 *
 */
#ifndef CONFFILES_H
#define CONFFILES_H

#include <sys/types.h>

/* XXX do I really need to reinvent this or can we share with namenode flags? */
typedef enum {
	conff_db_old=0x1,
	conff_db_cur=0x2,
	conff_db_new=0x4,
} conff_flag;

/* register a new conffile for package pkg at path... um... path, reading
 * the contents of the distributed conffile from fd, which is expected to
 * be sz bytes
 *
 * after calling this function it can be expected that the conffile is
 * "registered" as a new conffile, which will be later processed in the
 * package configuration stage, where it will be checked for stuff like
 * new/changed/obsoleted/etc.
 */
void conff_reg_fd(const char *pkg, const char *path, int fd, size_t sz);

/* return an fd opened for reading the contents of the "registered" conffile 
 * which is expected to be installed at path... path from package pkg.  the 
 * flag f is used to determine which version of the conffile is requested 
 * (i.e. old/new/current)
 */
int conff_get_fd(const char *pkg, const char *path, conff_flag f);

/* remove the on-disk conffile database for package pkg, using the flag f
 * to determine which version of the database to remove (new/cur/old)
 */
void conff_db_remove(const char *pkg, conff_flag f);

/* update the conffiles db so that the latest "new" files are now recorded
 * as "current" for package pkg.
 */
void conff_commit_new(const char *pkg);

/* attempt to automagically merge the 3-way delta for conffile at pathname
 * passes the return value of diff3, aka nonzero on merge failure
 */
/* XXX remember to check for symlink corner cases */
int conff_automerge(const char *pkg, const char *pathname);

 /* return the path of the archived config file.
  *
  */
 
char* conff_db_path(const char *pkg, const char *path, conff_flag f);

#endif /* CONFFILES_H */

