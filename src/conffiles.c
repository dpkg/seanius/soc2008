#include "conffiles.h"

#include <config.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <dpkg.h>
#include <dpkg-db.h>
#include "main.h"

extern int errno;

/* MD5 string length, minus null byte.  mostly here for readability below */
#define MD5SUM_LEN 32

/* calculate the md5sum of the contents of the null-terminated string input 
 * putting the result in output, which will be allocated with memory to
 * store the result.  your job to free it later.
 */
static void conff_md5sum_str(const char *input, char **output){
  buffer_arg arg = { (void*) output };
  struct buffer_data data = { buffer_write, arg, BUFFER_WRITE_MD5 };
  data.type|=BUFFER_WRITE_SETUP;
  data.proc(&data, NULL, 0, "conff_md5sum_str");
  data.type=BUFFER_WRITE_MD5;
  data.proc(&data, (void*)input, strlen(input), "conff_md5sum_str");
  data.type|=BUFFER_WRITE_SHUTDOWN;
  data.proc(&data, NULL, 0, "conff_md5sum_str");
}

/* return a pointer to a char* which has the full on-disk location of
 * the file in the "conffile db" registered for the file at path.  pass
 * flags f to indicate whether you want the "new", "old", or "current" version.
 *
 * no checking is done that the file actually exists, this is just where it
 * ought to be.  your job to free() the result.
 */
char* conff_db_path(const char *pkg, const char *path, conff_flag f){
  char *p = NULL, *hash = NULL;
  const char *conff_db = NULL;
  size_t root_sz = 0, path_sz = 0;
  
  /* which version of the db was requested? */
  switch(f){
  case conff_db_new:
  	conff_db = CONFFILESDIRTMP;
    break;
  case conff_db_cur:
  	conff_db = CONFFILESDIR;
    break;
  default:
    ohshit("conff_db_path called with unsupported flags %x", f);
    break;
  }

  /* <admindir>/<conffilesdb/><pkg> + '\0' */
  root_sz = strlen(admindir)+1+strlen(conff_db)+strlen(pkg)+1;
  path_sz = root_sz;

  /* and if a conffile is being requested (not just the db root)... */
  if(path!=NULL) {
  	path_sz+=(1+MD5SUM_LEN); /* / + hash */
  } 

  p = m_malloc(path_sz);

  /* this is the path to the conffile db root for pkg */
  snprintf(p, root_sz, "%s/%s%s", admindir, conff_db, pkg);

  /* append the pathname's hash if relevant */
  if(path!=NULL) {
	conff_md5sum_str(path, &hash);
  	snprintf(p+root_sz-1, 1+MD5SUM_LEN+1, "/%s", hash );
	free(hash);
  }

  printf("conff_db_path(%s, %s, %x) = %s\n", pkg, path, f, p);
  return p;
}

/* ensure that the <admindir>/<conffilesdb/><pkg> exists */
void conff_ensure_db(const char *pkg, conff_flag f){
  struct stat s;
  char *dbdir=NULL;
  short dirsmade=0;
  size_t base_sz=0; /* location of the / in <conffilesdb/> before <pkg> */

  dbdir = conff_db_path(pkg, NULL, f);
  /* to avoid an extra malloc() we reuse the same string twice */
  base_sz = strlen(dbdir) - (strlen(pkg)+1);
  /* and use a creative for loop to prevent a need for copy/paste :) */
  for(dbdir[base_sz] = '\0'; dirsmade!=2; dirsmade++) {
    printf("conff_ensure_db: creating %s\n", dbdir);
    if(stat(dbdir, &s)) {
      if(errno!=ENOENT) ohshite("conff_ensure_db: stat(%s)", dbdir);
      if(mkdir(dbdir, S_IRWXU)) ohshite("conff_ensure_db: mkdir(%s)", dbdir);
    }
	dbdir[base_sz]='/';
  }

  free(dbdir);
}

void conff_reg_fd(const char *pkg, const char *path, int fd, size_t sz){
  int cfgfd;
  /* get the path to where the registered file goes */
  char *p = conff_db_path(pkg, path, conff_db_new);
  char fnamebuf[256];

  printf("conffile_reg_fd: %s, %s, %s\n", pkg, path, p);
  /* create any leading components */
  conff_ensure_db(pkg, conff_db_new);
  /* make a mode 600 copy of file to p */
  cfgfd = open(p, (O_CREAT|O_EXCL|O_WRONLY), (S_IRUSR|S_IWUSR));
  if (cfgfd < 0) ohshite(_("unable to create `%.255s'"),p);
  fd_fd_copy(fd, cfgfd, sz, _("backend dpkg-deb during `%.255s'"),
             quote_filename(fnamebuf,256,p));
  if(close(cfgfd)) ohshite("can't close %s", p);
  free(p);
}

int conff_get_fd(const char *pkg, const char *path, conff_flag f){
  int fd=-1;
  char *p = conff_db_path(pkg, path, f);
  if( (fd=open(p, O_RDONLY)) == -1){
    ohshite("error opening conffile registered at %s", p);
  }
  free(p);
  return fd;
}

void conff_commit_new(const char *pkg){
  /* update the conffiles "db" dir.  this consists of the following steps:
   * - nuke the existing current db dir
   * - move the new db into place
   */
  char *cfgdb=NULL, *cfgdbnew=NULL;

  cfgdb = conff_db_path(pkg, NULL, conff_db_cur);
  cfgdbnew = conff_db_path(pkg, NULL, conff_db_new);

  /* make sure <admindir>/<conffdb/>/<pkg> exists, and then nuke <pkg> */
  conff_ensure_db(pkg, conff_db_cur);
  printf("conff_commit_new: rm -rf %s\n", cfgdb);
  ensure_pathname_nonexisting(cfgdb);
  printf("conff_commit_new: mv %s %s\n", cfgdbnew, cfgdb);
  if(rename(cfgdbnew, cfgdb))
  	ohshite("conff_commit_new: rename(%s,%s)", cfgdbnew, cfgdb);

  free(cfgdb);
  free(cfgdbnew);
}

void conff_db_remove(const char *pkg, conff_flag f){
  char *cfgdb= conff_db_path(pkg, NULL, f);
  printf("conff_db_remove(%s): removing %s\n", pkg, cfgdb);
  ensure_pathname_nonexisting(cfgdb);
  free(cfgdb);
}

int conff_automerge(const char *pkg, const char *pathname){
  int res, tmpfd, mrgfd;
  char tmpfn[] = "dpkg-merge.XXXXXX"; /* temporary file name */
  size_t cmdsz=0;
  char *cmdbuf = NULL, *cnfold = NULL, *cnfnew = NULL;

  /* the 'old' version is actually the current dist version in the db */
  cnfold=conff_db_path(pkg, pathname, conff_db_cur);
  cnfnew=conff_db_path(pkg, pathname, conff_db_new);

  if( (tmpfd = mkstemp(tmpfn)) == -1 ){
    ohshite("conff_automerge: mkstemp failed");
  }
  if( fchmod(tmpfd, (S_IRUSR|S_IWUSR)) || close(tmpfd) ) {
    ohshite("conff_automerge: chmod/close failed");
  }

  /* DIFF3 + " -m " + new + " " + old + " " + cur + " > " + tmpfile + '\0' */
  cmdsz = strlen(DIFF3) + 4 + strlen(cnfnew) + 1 + strlen(cnfold) 
          + 1 + strlen(pathname) + 3 + strlen(tmpfn) + 1;
  cmdbuf = m_malloc(cmdsz);
  snprintf(cmdbuf, cmdsz, "%s -m %s %s %s > %s", 
           DIFF3, cnfnew, cnfold, pathname, tmpfn);

  /* let's give it a go! */
  if( (res = system(cmdbuf)) == 0 ){
    /* copy the merge result by fd->fd, to respect symlinks */
    if( (tmpfd = open(tmpfn, O_RDONLY)) == -1 ) {
      ohshite("conff_automerge: re-opening merge tmpfile %s failed", tmpfn);
    }
    if( (mrgfd = open(pathname, O_WRONLY)) == -1 ) {
      ohshite("conff_automerge: opening %s for merging failed", pathname);
    }
    fd_fd_copy(tmpfd, mrgfd, -1, "merging conffile %s", pathname);
    if( close(tmpfd) || close(mrgfd) ){
      ohshite("conff_automerge: closing merge files for %s failed", pathname);
    }
  }

  if(unlink(tmpfn)){
    ohshite("conff_automerge: unlink merge tmpfile failed");
  }

  free(cmdbuf);
  free(cnfnew);
  free(cnfold);

  return res;
}

